from peewee import *

db = PostgresqlDatabase('developerdb',
                        user='developer',
                        password='developer',
                        autorollback=True)


class BaseModel(Model):
    class Meta:
        database = db


class Transaction(BaseModel):
    app_orderid = TextField()
    bitmex_orderid = TextField()
    side = TextField()
    orderqty = DecimalField(null=True)
    orderstatus = TextField()
    cum_orderqty = DecimalField()
    transacttime = DateTimeField()
# db.drop_tables([Transaction])
# db.create_tables([Transaction])


 