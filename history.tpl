<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>History</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/semantic-ui@2.3.2/dist/semantic.min.css" integrity="sha256-Z8Bv3UrxpRsWTfTPIjCojw5CdRNYSPw3TyxoU8WZSrM=" crossorigin="anonymous">
<style>
    .flexbox-site {
        display: flex;
        min-height: 100vh;
        flex-direction: column;
    }

    .flexbox-site-content {
        flex: 1;
    }
</style>
</head>
<body class="flexbox-site">
<div class="ui attached huge menu">
    <div class="ui container">
            <div class="header item">BitMEX client</div>
            
    </div>
</div>

<div class="ui hidden divider"></div>
<div class="ui container flexbox-site-content">
    <h1>Transaction History</h1>
    <table class="ui celled table">
        <thead>
          <tr>
            <th>Bitmex order id</th>
            <th>Side</th>
            <th>Order qty</th>
            <th>Order status</th>
            <th>Order qty 2</th>
            <th>Transact time</th>
            <th title='order id for app transactions'>Order id</th>
          </tr>
        </thead>
        <tbody>
        %for record in records:
          <tr>            
            <td>{{record.bitmex_orderid}}</td>
            <td>{{record.side}}</td>
            <td>{{record.orderqty}}</td>
            <td>{{record.orderstatus}}</td>
            <td>{{record.cum_orderqty}}</td>
            <td>{{record.transacttime}}</td>            
            <td>{{record.app_orderid}}</td>            
          </tr>
        %end
        </tbody>
      </table>
</div>
<div class="ui hidden divider"></div>
    <div class="ui inverted attached menu">
    

</div>
</body>
</html>

