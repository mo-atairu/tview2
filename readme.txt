HOW TO RUN:

1. You have python installed
2. You have pipenv installed.
    - none ! do; pip install pipenv
2(i). You have python 3.6 installed
    - none, go to https://conda.io/miniconda.html
3. Now run pipenv install, to install the dependencies
4. on first run do; python main.py --noauth_local_webserver; to authenticate with google
5. Profit.
6. After authentication. Just run with python main.py
7. Stop with control C.
8  Take a look at tmux or screen for simple background processesing