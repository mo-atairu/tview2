from bottle import template, Bottle, request, redirect
from model import Transaction

app = Bottle()

@app.route('/history')
def history():
    records = Transaction.select().limit(30).execute()
    return template('history', records=records)

app.run(host='0.0.0.0', port=8080, debug=True)